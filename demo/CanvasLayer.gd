extends CanvasLayer

@onready var main_menu_2 = $MainMenu2
@onready var settings = $Settings


func _on_button_play_pressed():
	get_tree().change_scene_to_file("res://main.tscn")

func _on_button_demo_pressed():
	get_tree().change_scene_to_file("res://demo.tscn")

func _on_button_settings_pressed():
	main_menu_2.visible = false
	settings.visible = true


func _on_button_quit_pressed():
	get_tree().quit()


func _on_button_settings_back_pressed():
	main_menu_2.visible = true
	settings.visible = false

#include "one_way_platform.h"
#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void OneWayPlatform::_bind_methods() {
    ClassDB::bind_method(D_METHOD("get_normal"), &OneWayPlatform::get_normal);
    ClassDB::bind_method(D_METHOD("set_normal", "norm"), &OneWayPlatform::set_normal);
    ClassDB::add_property("OneWayPlatform", PropertyInfo(Variant::VECTOR3, "normal", PROPERTY_HINT_RANGE, "0,20,0.01"), "set_normal", "get_normal");
}

OneWayPlatform::OneWayPlatform() {
    // Initialize any variables here.
    time_passed = 0.0;
    normal = Vector3(0,1,0);
}

OneWayPlatform::~OneWayPlatform() {
    // Add your cleanup here.
}

void OneWayPlatform::_process(double delta) {
    time_passed += delta;

}

void OneWayPlatform::set_normal(const Vector3 norm) {
    normal = norm;
}

Vector3 OneWayPlatform::get_normal() const {
    return normal;
}

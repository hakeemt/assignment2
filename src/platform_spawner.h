#ifndef PLATFORM_SPAWNER_H
#define PLATFORM_SPAWNER_H

#include <godot_cpp/classes/area3d.hpp>
#include <godot_cpp/godot.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/resource_loader.hpp>
// #include <demo/platform.tscn>

namespace godot {

class PlatformSpawner : public Node3D {
    GDCLASS(PlatformSpawner, Node3D)

private:
    double time_passed;
    float spacing;
    int platforms;
    String platform_scene_path = "res://platform.tscn";
    String spring_platform_scene_path = "res://spring_platform.tscn";


protected:
    static void _bind_methods();

public:
    PlatformSpawner();
    ~PlatformSpawner();

    void _ready();
    void _init();
    void _process(double delta);
    void onPlatformSpawnerAreaEntered(const Area3D* area);
    void set_spacing(const float pull);
    float get_spacing() const;
    void set_platforms(const int num_platforms);
    int get_platforms();
};

}

#endif

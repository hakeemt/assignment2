#ifndef ONE_WAY_PLATFORM
#define ONE_WAY_PLATFORM_H

#include <godot_cpp/classes/area3d.hpp>

namespace godot {

class OneWayPlatform : public Area3D {
    GDCLASS(OneWayPlatform, Area3D)

private:
    double time_passed;
    Vector3 normal;

protected:
    static void _bind_methods();

public:
    OneWayPlatform();
    ~OneWayPlatform();

    void _process(double delta);
    void set_normal(const Vector3 norm);
    Vector3 get_normal() const;
};

}

#endif

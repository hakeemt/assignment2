#include "platform_spawner.h"
#include "one_way_platform.h"


#include <godot_cpp/classes/input.hpp>
#include <godot_cpp/classes/packed_scene.hpp>
#include <godot_cpp/classes/random_number_generator.hpp>

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/engine.hpp>

using namespace godot;

void PlatformSpawner::_bind_methods() {
    // ClassDB::bind_method(D_METHOD("onPlatformSpawnerAreaEntered", "area"), &PlatformSpawner::onPlatformSpawnerAreaEntered);
    ClassDB::bind_method(D_METHOD("get_spacing"), &PlatformSpawner::get_spacing);
    ClassDB::bind_method(D_METHOD("set_spacing", "platform_spacing"), &PlatformSpawner::set_spacing);
    ClassDB::add_property("PlatformSpawner", PropertyInfo(Variant::FLOAT, "spacing", PROPERTY_HINT_RANGE, "0,20,0.01"), "set_spacing", "get_spacing");
    ClassDB::bind_method(D_METHOD("get_platforms"), &PlatformSpawner::get_platforms);
    ClassDB::bind_method(D_METHOD("set_platforms", "num_platforms"), &PlatformSpawner::set_platforms);
    ClassDB::add_property("PlatformSpawner", PropertyInfo(Variant::INT, "platforms", PROPERTY_HINT_RANGE, "0,100,1"), "set_platforms", "get_platforms");
}

PlatformSpawner::PlatformSpawner() {
    time_passed = 0.0;
    spacing = 3.5;
    platforms = 50;
}

PlatformSpawner::~PlatformSpawner() {
    // Add your cleanup here.
}

void PlatformSpawner::_ready() {
    RandomNumberGenerator rng;
    rng.randomize();

    //Spawn Platforms
    godot::ResourceLoader* resource_loader = godot::ResourceLoader::get_singleton();
    Ref<godot::PackedScene> platform_scene = resource_loader->load(platform_scene_path);
    Ref<godot::PackedScene> spring_platform_scene = resource_loader->load(spring_platform_scene_path);

    if(platform_scene.is_null()){
        UtilityFunctions::print("Failed to Load Platform\n");
        return;
    }
    float height = spacing;
    float prev_x = 0.0;
    float prev_z = 0.0;
    for(int i = 0; i < platforms; i++){
        bool is_normal_platform = rng.randf_range(0.0,1.0) < 0.8;
        Node* platform_node;
        if(is_normal_platform)
            platform_node = platform_scene->instantiate();
        else
            platform_node = spring_platform_scene->instantiate();

        Node3D* platform = Object::cast_to<Node3D>(platform_node);
        if(platform == nullptr){
            UtilityFunctions::print("Failed to Cast Platform\n");
            return;
        }

        add_child(platform);
        UtilityFunctions::print("Added Platform\n", i);
        float len = rng.randf_range(-5.0, 5.0);
        float rot = rng.randf_range(0.0, 6.28);
        prev_x += len * cos(rot);
        prev_z += len * sin(rot);

        platform->set_position(Vector3(prev_x, height, prev_z));
        if(is_normal_platform)
            height += spacing;
        else
            height += spacing*3;
    }
}

void PlatformSpawner::_init(){

}

void PlatformSpawner::_process(double delta) {

    if (Engine::get_singleton()->is_editor_hint()) return;
}

void PlatformSpawner::set_spacing(const float platform_spacing) {
    spacing = platform_spacing;
}

float PlatformSpawner::get_spacing() const {
    return spacing;
}

void PlatformSpawner::set_platforms(const int num_platforms){
    platforms = num_platforms;
}

int PlatformSpawner::get_platforms(){
    return platforms;
}
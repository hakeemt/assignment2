#ifndef PLAYER_H
#define PLAYER_H

#include <godot_cpp/classes/area3d.hpp>
#include <godot_cpp/godot.hpp>
#include <godot_cpp/classes/node.hpp>
#include <godot_cpp/classes/resource_loader.hpp>
// #include <demo/platform.tscn>

namespace godot {

class Player : public Area3D {
    GDCLASS(Player, Area3D)

private:
    double time_passed;
    Vector3 velocity;
    float pull;
    bool strafe_mode;
    bool can_press;
    bool can_double_jump;
    float angle;
    String platform_scene_path = "res://platform.tscn";

protected:
    static void _bind_methods();

public:
    Player();
    ~Player();

    void _ready();
    void _process(double delta);
    void onPlayerAreaEntered(const Area3D* area);
    void set_pull(const float pull);
    float get_pull() const;
    void set_strafe_mode(const bool is_strafing);
    bool get_strafe_mode();
};

}

#endif

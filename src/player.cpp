#include "player.h"
#include "one_way_platform.h"


#include <godot_cpp/classes/input.hpp>
#include <godot_cpp/classes/packed_scene.hpp>
#include <godot_cpp/classes/scene_tree.hpp>

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/engine.hpp>

using namespace godot;

void Player::_bind_methods() {
    ClassDB::bind_method(D_METHOD("onPlayerAreaEntered", "area"), &Player::onPlayerAreaEntered);
    ClassDB::bind_method(D_METHOD("get_pull"), &Player::get_pull);
    ClassDB::bind_method(D_METHOD("set_pull", "pull_force"), &Player::set_pull);
    ClassDB::add_property("Player", PropertyInfo(Variant::FLOAT, "pull", PROPERTY_HINT_RANGE, "0,2,0.01"), "set_pull", "get_pull");
    ClassDB::bind_method(D_METHOD("get_strafe_mode"), &Player::get_strafe_mode);
    ClassDB::bind_method(D_METHOD("set_strafe_mode", "is_strafing"), &Player::set_strafe_mode);
    ClassDB::add_property("Player", PropertyInfo(Variant::FLOAT, "strafe_mode", PROPERTY_HINT_RANGE, "0,20,0.01"), "set_strafe_mode", "get_strafe_mode");
}

Player::Player() {
    time_passed = 0.0;
    velocity = Vector3(0,0,0);
    pull = 0.5;
    strafe_mode = false;
}

Player::~Player() {
    // Add your cleanup here.
}

void Player::_ready() {
    Vector3 starting_position = Vector3(0, 2, 0); //Vector3((rand() % 8000 - 4000)/ 1000.0, (rand() % 8000 - 4000)/ 1000.0, (rand() % 8000 - 4000)/ 1000.0);

    float x = (rand() % 2000 - 1000)/ 1000.0;
    float y = (rand() % 2000 - 1000)/ 1000.0;
    float z = (rand() % 2000 - 1000)/ 1000.0;

    float mag = sqrt(x*x + y*y + z*z);

    velocity = Vector3(0, 0.2, 0);
    set_position(starting_position);

    this->connect("area_entered", Callable(this, StringName("onPlayerAreaEntered")));
    UtilityFunctions::print("READY\n");


    //Spawn Platforms
    // godot::ResourceLoader* resource_loader = godot::ResourceLoader::get_singleton();
    // Ref<godot::PackedScene> platform_scene = resource_loader->load(platform_scene_path);

    // if(platform_scene.is_null()){
    //     UtilityFunctions::print("Failed to Load Platform\n");
    //     return;
    // }
    // for(int i = 0; i < 100; i++){
    //     Node* platform_node = platform_scene->instantiate();
    //     Node3D* platform = Object::cast_to<Node3D>(platform_node);
    //     if(platform == nullptr){
    //         UtilityFunctions::print("Failed to Cast Platform\n");
    //         return;
    //     }

    //     add_child(platform);
    //     UtilityFunctions::print("Added Platform\n", i);
    //     platform->set_position(Vector3(0.0, i + 3.0, 0.0));
    // }
}

void Player::_process(double delta) {

    if (Engine::get_singleton()->is_editor_hint()) return;

    godot::Input* input = godot::Input::get_singleton();

    if (input->is_key_pressed(KEY_ENTER) && can_press){
        UtilityFunctions::print("Toggled Strafe");
        if(strafe_mode)
            strafe_mode = false;
        else
            strafe_mode = true;
        can_press = false;
    }
    if(input->is_key_pressed(KEY_ENTER) == false){
        can_press = true; //allows key press to only trigger once until key is released
    }

    // Check for player input (e.g., arrow keys or WASD).
    if (input->is_key_pressed(KEY_D)) {
        if(strafe_mode){
            velocity.x -= 1/60.0;
        }
        else{
            angle -= 0.04;
        }
    }
    if (input->is_key_pressed(KEY_A)) {
        if(strafe_mode){
            velocity.x += 1/60.0;
        }
        else{
            angle += 0.04;
        }
        
    }
    if (input->is_key_pressed(KEY_W)) {
        velocity.z += 1/60.0;
    }
    if (input->is_key_pressed(KEY_S)) {
        velocity.z -= 1/60.0;
    }

    if(input->is_key_pressed(KEY_SPACE) && can_double_jump){
        velocity.y = 20/60.0;
        can_double_jump = false;
    }

    velocity.x *= 0.8;
    velocity.z *= 0.8;

    //Gravity

    time_passed += delta;

    velocity.y -= pull/60.0;

    float new_x = get_position().x + velocity.x * cos(angle) + velocity.z * sin(angle);
    float new_z = get_position().z + velocity.z * cos(angle) + velocity.x * sin(angle) * -1;
    Vector3 new_position = Vector3(new_x, get_position()[1] + velocity[1], new_z);
    Vector3 new_rotation = Vector3(get_rotation().x, angle, get_rotation().z);

    set_position(new_position);
    set_rotation(new_rotation);

    if(get_position().y < -50){
        UtilityFunctions::print("Reset");
        set_position(Vector3(0,2,0));
        velocity = Vector3(0,0.2,0);
        get_tree()->change_scene_to_file("res://menu.tscn");
    }
}


void Player::onPlayerAreaEntered(const Area3D* area){

    Vector3 norm;

    //If null, then wall
    if(Object::cast_to<Player>(area) == nullptr){
        const OneWayPlatform * wall = Object::cast_to<OneWayPlatform>(area);
        norm = wall -> get_normal();

    }
    else
    {
        norm = (get_position() - area -> get_position()).normalized();
        
    }
    UtilityFunctions::print("Bounce\n");
    // R = I - 2N(N·I)
    if(velocity.y < 0){
        velocity.y = 20/60.0 * norm.y; // (velocity - 2 * norm * norm.dot(velocity));
        can_double_jump = true;
        UtilityFunctions::print("Norm y: %f\n", norm.y);
    }


}

void Player::set_pull(const float pull_force) {
    pull = pull_force;
}

float Player::get_pull() const {
    return pull;
}

void Player::set_strafe_mode(const bool is_strafing){
    strafe_mode = is_strafing;
}

bool Player::get_strafe_mode(){
    return strafe_mode;
}
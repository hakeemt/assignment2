# SkywardSketch

## Table of Contents
- [Project Demo](#project-demo)
- [Installation](#installation)
- [Game Design](#game-design)
- [Software Architecture and Planning](#software-architecture-and-planning)
- [Division of Labor](#division-of-labor)


---
## Project Demo:
![SkyWard Demo](./SkywardSketch.mp4)


---
## Installation:

### Prerequisites:

Ensure you have the Godot engine and the necessary C++ bindings. If you're unfamiliar with the process, you can follow the GDExtension tutorial for a detailed guide.

## Clone the Repository
You can choose to either set up a repo via SSH or HTTPS:

### Using SSH:
    git clone git@gitlab.com:hakeemt/assignment2.git

### Using HTTPS:
    git clone https://gitlab.com/hakeemt/assignment2.git
    
### Set Up a Virtual Environment:

    python -m venv venv
    source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
    
### Install Dependencies:
    pip install SCons

### Building the C++ bindings:

1. **Generate Extension API File (if needed)**: If you're using a newer version of Godot, generate the extension API file:
    ```bash
    godot --dump-extension-api extension_api.json
    ```
    Place the `extension_api.json` file in the project folder.

2. **Compile the Bindings**: Navigate to the `godot-cpp` directory and compile the bindings. Replace `<platform>` with your OS (windows, linux, or macos). To speed up the compilation, add `-jN` at the end, where N is the number of CPU threads on your system (e.g., 4 threads).
    
        cd godot-cpp
        scons platform=<platform> -j4 custom_api_file=<PATH_TO_FILE>
        cd ..

---

## Game Design
### Overview
The game is a 3D adaptation of "Doodle Jump". Players aim to jump from one generated platform to another, striving to reach as high as possible. As players progress, platforms below a certain threshold disappear. The game introduces custom platforms such as springs for higher jumps and jump pads that launch the player into a glide state. The challenge ends if the player falls out of the world.

### Scoring Rules
Players earn scores based on the maximum altitude they achieve.

### Visual Design
Incorporating a doodly/geometric aesthetic, the game features geometrically-styled players, platforms, and springs.

### GUI Design
The GUI prominently showcases the player's score and any collected items or stat enhancements during gameplay.

### Sound Design
The game will feature a custom background track to enhance the playing experience.

### Gameplay Elements
Throughout the game, players can collect items that buff stats, including agility, speed, and jump height.

---

## Software Architecture and Planning
### Structuring
- The game is anchored around a central `Node3D`, setting the stage for all gameplay elements.
- Players navigate a series of platforms, primarily the `OneWayPlatform`, jumping from one to another.
- Special platforms like `JumpBoostOneWay` offer heightened jumps, adding an element of surprise.
- A spherical `Player` node is the game's protagonist. The camera's attachment to the player ensures an immersive follow-view gameplay.
- Dynamic lighting based on the player's position, enhancing the visual experience.

### Future Enhancements
- Implementation of randomly generated platforms for unique gameplay sessions.
- Transition to a never-ending game loop, increasing the challenge as players ascend.


### Modularity
- **Scene Changes**:
    - **Branch**: `feature/scene-updates`
    - Aim: Isolate and test scene modifications without affecting the main codebase.

- **GDExtensions**:
    - **Branch**: `feature/gdextensions`
    - Aim: Add or update GDExtensions without disrupting other game components.

- **Advantages**:
    - **Isolation**: Changes in one branch won't impact the main code or other branches.
    - **Targeted Testing**: Test changes specific to a branch.
    - **Efficient Reviews**: Simplify code reviews by narrowing focus.
    - **Parallel Work**: Team members can work on different aspects simultaneously.
### Game State Management
(To be determined)


---

## Scene Structure

### Main Node
**Node Name**: `main`
**Type**: `Node3D`
- The root node of the scene, serving as the base structure from which all other nodes and gameplay elements originate.

### OneWayPlatform
**Parent**: `main`
- **Children**:
  - **CollisionShape3D**: Defines the physical shape for collision detection, ensuring the platform interacts appropriately with other objects.
  - **MeshInstance3D**: Gives the platform its visual appearance using the Cylinder Mesh and for its surface material.
- Represents standard platforms where the player can land and jump from. It's equipped with attributes for both collision and appearance.

### JumpBoostOneWay
**Parent**: `main`
- **Children**:
  - **CollisionShape3D**: Very similar to OneWayPlatform.
  - **MeshInstance3D**: Very similar to OneWayPlatform's mesh, but a different shade of green.
- A specialized platform that, when interacted with, propels the player to greater heights, offering unique gameplay dynamics.

### Player
**Parent**: `main`
- **Children**:
  - **CollisionShape3D**: Uses the Sphere Shape to define the player's boundaries.
  - **MeshInstance3D**: Determines the player's visual representation using the Sphere Mesh mesh and Standard Material.
  - **Camera3D**: Provides the player's perspective in the game with a specific transform attribute.
  - **OmniLight3D**: Adds lighting effects relative to the player's position.
- The main character of the game, equipped with attributes that govern movement, appearance, and interactions within the game world.

## Division of Labor
**Hakeem**: Responsible for organization of meetings, documentation, and helping with code quality.

**Cooper**: Responsible for code quality, functionality, and scene structure. 

**Cristian**: Responsible for graphics and design.

- We plan to meet on Discord and code together, usually in the late afternoons.
